#include <Arduino.h>
#include "DEngine.h"
#include "DLora.h"
#include "DPackage.h"

DEngine* Engine;

void OnReceive(int PackageSize)
{
    if(PackageSize)
    {
        String InMessage("", PackageSize);

        if(LoRa.available())
        {
            LoRa.readBytes(InMessage.begin(), PackageSize);

            if(InMessage == GET_STATE)
            {
                DLora::SendMessage(Engine->GetEngineState());
            }
            else if(InMessage == START_ENGINE)
            {
                Engine->Start();
            }
            else if(InMessage == STOP_ENGINE)
            {
                Engine->Start();
            }
        }
    }
}

void setup()
{
    Serial.begin(9600);
    DLora::InitLoRa(OnReceive);

    Engine = new DEngine(25, 33, 32, 4);
    Engine->BindMessages(DLora::SendMessage);
}

void loop()
{
    Engine->RunStarter();
}