#ifndef RECIEVER_DENGINE_H
#define RECIEVER_DENGINE_H

#include "DPackage.h"

enum class EEngineState
{
    ACC,
    ON,
    WORK
};

class DEngine
{
public:
    explicit DEngine(int InHandBreakPin, int InTransmissionPin, int InIgnitionPin, int InStarterPin);

    String GetEngineState() const;

    void Start();

    void RunStarter();

    void BindMessages(void (*InMessageCallback)(const String&)) { MessageCallback = InMessageCallback; }

private:
    void (*MessageCallback)(const String&) = nullptr;

    const int MAX_STARTER_TIME = 800;
    unsigned long CurrentTime = 0;

    EEngineState CurrentState = EEngineState::ACC;

    int HandBreakPin;
    int TransmissionPin;

    int IgnitionPin;
    int StarterPin;

    void BroadcastMessage(const String& InMessage);
    void SetStarterEnabled(bool IsEnabled);

    bool CanRunStarter() const;
};

#endif
