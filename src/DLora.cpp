#include "DLora.h"

void DLora::InitLoRa(ReceiveCallback Callback)
{
    LoRa.setPins(18, 14, 26);
    while(!LoRa.begin(866E6))
    {
        delay(500);
    }

    LoRa.setSyncWord(0xF3);

    /*LoRa.setPreambleLength(16);
    LoRa.setTxPower(20);
    LoRa.setSignalBandwidth(31.25E3);
    LoRa.setCodingRate4(8);
    LoRa.setSpreadingFactor(11);*/

    LoRa.onReceive(Callback);
    LoRa.receive();
}

void DLora::SendMessage(const String& InMessage)
{
    LoRa.beginPacket();
    LoRa.print(InMessage);
    LoRa.endPacket(true);
    LoRa.receive();
};
