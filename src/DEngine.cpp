#include "DEngine.h"
#include "Arduino.h"

DEngine::DEngine(int InHandBreakPin, int InTransmissionPin, int InIgnitionPin, int InStarterPin)
    : HandBreakPin(InHandBreakPin), TransmissionPin(InTransmissionPin), IgnitionPin(InIgnitionPin), StarterPin(InStarterPin)
{
    pinMode(HandBreakPin, INPUT);
    pinMode(TransmissionPin, INPUT);
    pinMode(IgnitionPin, OUTPUT);
    pinMode(StarterPin, OUTPUT);
}

String DEngine::GetEngineState() const
{
    switch(CurrentState)
    {
    case EEngineState::ACC: return ACC_STATE; break;
    case EEngineState::ON: return ON_STATE; break;
    case EEngineState::WORK: return WORK_STATE; break;
    }
    return ERROR;
}

void DEngine::Start()
{
    if(CurrentState == EEngineState::ACC)
    {
        // TODO Включение реле зажигания
        CurrentState = EEngineState::ON;
        BroadcastMessage(GetEngineState());
    }
    else if(CurrentState == EEngineState::ON)
    {
        CanRunStarter() ? SetStarterEnabled(true) : BroadcastMessage(START_ERROR);
    }
}

void DEngine::RunStarter()
{
    if(digitalRead(StarterPin) && millis() - CurrentTime > MAX_STARTER_TIME)
    {
        SetStarterEnabled(false);

        // TODO проверка завелся ли двигатель
        CurrentState = EEngineState::WORK;

        BroadcastMessage(GetEngineState());
    }
}

void DEngine::BroadcastMessage(const String& InMessage)
{
    if(MessageCallback)
        MessageCallback(InMessage);
}

void DEngine::SetStarterEnabled(bool IsEnabled)
{
    digitalWrite(StarterPin, IsEnabled);

    if(IsEnabled)
        CurrentTime = millis();
}
bool DEngine::CanRunStarter() const { return !digitalRead(StarterPin) && digitalRead(HandBreakPin) && !digitalRead(TransmissionPin); }
